import com.rabbitmq.client.*;

import java.io.IOException;

public class Admin {
    public static void main (String[] argv) throws Exception {
        System.out.println("Admin:");
        // connection & channel
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        // exchange
        String EXCHANGE_NAME = "clinic_exchange";
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        // queue & bind
        String queueName = "queue";
        channel.queueDeclare(queueName, false,false, false, null);
        String key = "*.*";
        channel.queueBind(queueName, EXCHANGE_NAME, key);

        // consumer (handle msg)
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received: " + message + " on key " + envelope.getRoutingKey());
            }
        };
        // start listening
        System.out.println("Waiting for messages...");
        channel.basicConsume(queueName, true, consumer);

    }
}
