import com.rabbitmq.client.*;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@SuppressWarnings("ALL")
public class Doctor {
    public static void main (String[] argv) throws Exception {

        // info
        System.out.println("Provide doctor name or number");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String doctorName = br.readLine();

        System.out.println("Doctor " + doctorName);

        // connection & channel
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        // exchange
        String EXCHANGE_NAME = "clinic_exchange";
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        // queue
        String queueName = doctorName;
        channel.queueDeclare(queueName, false,false, false, null);
        String key = "doctors." + doctorName;
        channel.queueBind(queueName, EXCHANGE_NAME, key);

        // consumer (handle msg)
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery (String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                        byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received: " + message);
            }
        };

        // start listening
        System.out.println("Waiting for messages...");
        channel.basicConsume(queueName, true, consumer);   //boolean is autoack

        System.out.println("Provide patient name and then requested examination or type exit to leave");
        String patientName = br.readLine();
        String requestedExamination;
        while (!patientName.equals("exit")) {
            requestedExamination = br.readLine();
            // publish
            String message = patientName + ";" + requestedExamination;
            key = doctorName + "." + requestedExamination;
            channel.basicPublish(EXCHANGE_NAME, key, null, message.getBytes("UTF-8"));
            System.out.println("Sent: " + message);

            System.out.println("Provide patient name and then requested examination or type exit to leave");
            patientName = br.readLine();

        }

    }
}
