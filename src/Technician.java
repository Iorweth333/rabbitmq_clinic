import com.rabbitmq.client.*;
import java.util.Random;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Technician {
    public static void main (String[] argv) throws Exception {
        // info
        System.out.println("Provide technician's specializations:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String spec1Q = br.readLine();
        String spec2Q = br.readLine();

        System.out.println("Technician " + spec1Q + " " + spec2Q);

        // connection & channel
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicQos(1);

        // exchange
        String EXCHANGE_NAME = "clinic_exchange";
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        // queue & bind
        channel.queueDeclare(spec1Q, false,false, false, null);
        String key = "*." + spec1Q;                                  //* is any one word
        channel.queueBind(spec1Q, EXCHANGE_NAME, key);
        channel.queueDeclare(spec2Q,false,false, false, null);
        key = "*." + spec2Q;
        channel.queueBind(spec2Q, EXCHANGE_NAME, key);


        // consumer (handle msg)
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery (String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                        byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received: " + message);
                int semicolonPos = message.indexOf(';');
                Random random = new Random();
                try {
                    int howManySeconds = random.nextInt(10) + 5;
                    System.out.println("Working for: " + howManySeconds);
                    Thread.sleep(howManySeconds * 1000);
                    //sleep random number of seconds (between 0 and 10 sec.)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //send response
                String key = envelope.getRoutingKey();
                int dotPos = key.indexOf('.');
                String doctorName = key.substring(0,dotPos);;
                String responseKey = "doctors." +doctorName;
                String response = message.concat(";done");
                channel.basicPublish(EXCHANGE_NAME, responseKey, null, response.getBytes("UTF-8"));
                System.out.println("Sent: " + response + " to doctor " + doctorName);
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        // start listening
        System.out.println("Waiting for messages in queue " + spec1Q);
        boolean autoAck = false;
        channel.basicConsume(spec1Q, autoAck, consumer);
        System.out.println("Waiting for messages in queue " + spec2Q);
        channel.basicConsume(spec2Q, autoAck, consumer);


    }
}
